{ config, pkgs, stdenv, lib, ... }:

with builtins;
with import ./lib.nix lib;
with lib;

let

  # Definitions
  # ===========================================================================

  cfg = config.deployment.lxd;
  formatKeyVal = k: v: strings.concatStringsSep "=" [ k (toString v) ];
  makeNixosConfig = import <nixpkgs/nixos>;
  makeSystemTarball = import <nixpkgs/nixos/lib/make-system-tarball.nix>;
  pkgs2storeContents = map (x: { object = x; symlink = "none"; });

  # The base image does not depend on the actual configuration of the container
  # so that it can be shared among different containers. It is only rebuilt when
  # nixpkgs is updated. This is why the base configuration is defined here, so
  # that we can use it both to generate the base image and to define the
  # configuration of the container.

  baseConfiguration = {
    nixpkgs.system = mkOverride 900 "x86_64-linux";
    boot.isContainer = true;

    # Disable some features not required in containers
    services.udisks2.enable = mkDefault false;
    security.polkit.enable = mkDefault false;
    powerManagement.enable = mkDefault false;

    # Make the container more compact by disabling documentation and X libraries
    documentation.enable = mkDefault false;
    documentation.nixos.enable = mkDefault false;
    documentation.man.enable = mkDefault false;
    documentation.info.enable = mkDefault false;
    environment.noXlibs = mkDefault true;

    # Make the container more compact by restricting the default locale set
    i18n.defaultLocale = mkDefault "en_US.UTF-8";
    i18n.supportedLocales = mkDefault [ "en_US.UTF-8/UTF-8" ];

    # Disable some systemd services known to fail in containers
    systemd.sockets.systemd-journald-audit.enable = false;
    systemd.sockets.systemd-udevd-kernel.enable = false;
    systemd.services.getty.enable = false;
    systemd.services.kmod-static-nodes.enable = false;
    systemd.paths.systemd-ask-password-wall.enable = false;

    # Install systemd in /sbin/init, where LXD will look for it
    system.activationScripts.installInitScript =
      ''ln -fs $systemConfig/init /sbin/init'';

    # XXX: when pluggable transports are implemented, we can make the
    # container's SSH optional and configure the container through the LXD API.
    services.openssh.enable = true;
    services.openssh.startWhenNeeded = false;
    services.openssh.extraConfig = "UseDNS no";
  };

  # We generate two different images, in addition to the metadata tarball : a
  # standard image and an image with an empty store used for containers with
  # shareStore.

  baseConfig = (makeNixosConfig { configuration = baseConfiguration; }).config;
  baseMetadata = pkgs.callPackage makeSystemTarball {
    contents = [{
    source = pkgs.writeText "metadata.yaml" ''
      architecture: ${strings.removeSuffix "-linux" currentSystem}
      creation_date: 1
      properties:
        description: "NixOps base image"
        os: NixOS
        release: ${baseConfig.system.nixos.release}
    '';
    target = "/metadata.yaml";
  }];};
  baseRootfs = pkgs.callPackage makeSystemTarball {
    contents = [];
    storeContents = [ {
      object = baseConfig.system.build.toplevel + "/init";
      symlink = "/sbin/init";
    } ] ++ pkgs2storeContents [ pkgs.stdenv ];
    extraCommands = "mkdir -p proc sys dev"; extraArgs = "--owner=0";
  };
  sharedRootfs = pkgs.callPackage makeSystemTarball {
    contents = [ {
      source = baseConfig.system.build.toplevel + "/init";
      target = "/sbin/init";
    } ];
    storeContents = [];
    extraCommands = "mkdir -p proc sys dev"; extraArgs = "--owner=0";
  };
in

{

  # Interface
  # ===========================================================================

  options = with types; {

    # Connection
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    deployment.lxd.endpoint = mkOption {
      type = nullOr str;
      default = null;
      example = "https://127.0.0.1:8443/";
      description = ''
        API endpoint URI, if not using the local socket.
      '';
    };

    deployment.lxd.sslCert = mkOption {
      type = nullOr str;
      default = null;
      description = ''
        Client SSL certificate file path.
      '';
    };

    deployment.lxd.sslKey = mkOption {
      type = nullOr str;
      default = null;
      description = ''
        Client SSL key file path.
      '';
    };

    deployment.lxd.sslVerify = mkOption {
      type = bool;
      default = true;
      description = ''
        Verify server SSL certificate.
      '';
    };

    deployment.lxd.sshPrivateKey = mkOption {
      type = nullOr str;
      default = null;
      description = ''
        Client SSH private key file path.
      '';
    };

    deployment.sshPrivateKey = mkOption {
      type = nullOr str;
      default = null;
      description = ''
        Client SSH public key file path.
      '';
    };

    # Storage
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    deployment.lxd.storagePool = mkOption {
      type = nullOr str;
      default = null;
      example = "default";
      description = ''
        The storage pool where the virtual disk is be created.
      '';
    };

    # TODO: implement
    deployment.lxd.storageLimits = mkOption { default = {}; type = submodule { options = {
      read = mkOption {
        type = nullOr str;
        default = null;
        example = "30MB";
        description = ''
          Maximum reads per second (by MB or Iops).
        '';
      };
      write = mkOption {
        type = nullOr str;
        default = null;
        example = "10MB";
        description = ''
          Maximum reads per second (by MB or Iops).
        '';
      };
    };};};

    # TODO: implement
    deployment.lxd.storagePriority = mkOption {
      type = nullOr int;
      default = null;
      description = ''
        Virtual storage device priority.
      '';
    };

    deployment.lxd.sharedStore = mkOption {
      type = bool;
      default = false;
      description = ''
        Share the host's store with the container, and copy the system closure
        to the host instead of the container.
      '';
    };

    # Resources
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    deployment.lxd.vcpu = mkOption {
      type = nullOr str;
      default = null;
      example = "0-3,7-11";
      description = ''
        Virtual CPU specification (core count or pinnings).
      '';
          };

    deployment.lxd.cpuPriority = mkOption {
      type = nullOr ints.positive;
      default = null;
      description = ''
        CPU Priority of the container.
      '';
    };

    deployment.lxd.cpuShare = mkOption {
      type = nullOr str;
      default = null;
      example = "25ms/200ms";
      description = ''
        CPU Time allowance (fixed share or time slices).
      '';
    };

    deployment.lxd.memorySize = mkOption {
      type = nullOr ints.positive;
      default = null;
      description = ''
        Memory size (M) of container.
      '';
    };

    deployment.lxd.memoryLimit = mkOption {
      type = enum [ "hard" "soft"];
      default = "hard";
      description = ''
        Memory limiting type for this container.
      '';
    };

    deployment.lxd.swap = mkOption {
      type = bool;
      default = true;
      description = ''
        Allow the kernel to swap this container's memory.
      '';
    };

    deployment.lxd.swapPriority = mkOption {
      type = nullOr ints.positive;
      default = null;
      description = ''
        Swapping priority for this container
      '';
    };

    # Image
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    deployment.lxd.baseImageMetadata = mkOption {
      type = path;
      example = "/home/alice/base-image-metadata.tar";
      description = ''Use the specified metadata tarball for the base image.'';
    };

    deployment.lxd.baseImageRootfs = mkOption {
      type = path;
      example = "/home/alice/base-image.tar";
      description = ''Use the specified rootfs tarball for the base image.'';
    };

    # LXD networks and profiles
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    # TODO: implement, allow routed networks
    deployment.lxd.networks = mkOption {
      default = [ ];
      type = listOf (resource "network");
      description = ''
        The names of the LXD networks to attach to this container.
      '';
    };

    # TODO: implement
    deployment.lxd.networkPriority = mkOption {
      type = nullOr ints.positive;
      default = null;
      description = ''
        Network priority for this container
      '';
    };

    deployment.lxd.profiles = mkOption {
      type = listOf str;
      default = [ "default" ];
      description = ''
        The names of the LXD profiles to apply to this container.
      '';
    };

    # Isolation (AppArmor, UID mapping)
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    deployment.lxd.apparmorProfile = mkOption {
      default = null;
      type = nullOr str;
      example = "unconfined";
      description = ''
        AppArmor profile name
      '';
    };

    deployment.lxd.isolated = mkOption {
      type = bool;
      default = true;
      description = ''
        Isolate this container from other containers.
      '';
    };

    deployment.lxd.idmapBase = mkOption {
      type = nullOr ints.positive;
      default = null;
      example = "1000000";
      description = ''User ID mapping base for this container.'';
    };

    deployment.lxd.idmapSize = mkOption {
      type = nullOr ints.positive;
      default = null;
      example = "1000000000";
      description = ''User ID mapping size for this container.'';
    };

    # Extra configuration
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    deployment.lxd.extraDevices = mkOption {
      type = attrsOf str;
      default = {};
      description = ''
        The devices of the container.'
      '';
    };

    deployment.lxd.extraConfig = mkOption {
      type = attrsOf str;
      default = {};
      description = ''
        The configuration of the container.
      '';
    };

    deployment.lxd.rawLxcConfig = mkOption {
      type = attrsOf str;
      default = {};
      description = ''
        Raw LXC options of the container.
      '';
    };

    deployment.lxd.finalConfig = mkOption {
      type = str;
      visible = false;
      internal = true;
      description = "Final LXD configuration.";
    };

  };

  # Implementation
  # ===========================================================================

  config = mkIf (config.deployment.targetEnv == "lxd") ({

    deployment.lxd.baseImageMetadata = mkDefault baseMetadata;
    deployment.lxd.baseImageRootfs =
      mkDefault (if cfg.sharedStore then sharedRootfs else baseRootfs);
    deployment.lxd.finalConfig = generators.toYAML {} {
      profiles = cfg.profiles;
      config = mapAttrs (key: toString) (filterAttrs (key: value: value != null) {
        "security.idmap.isolated" = cfg.isolated;
        "security.idmap.base" = cfg.idmapBase;
        "security.idmap.size" = cfg.idmapSize;
        "limits.read" = cfg.storageLimits.read;
        "limits.write" = cfg.storageLimits.write;
        "limits.disk.priority" = cfg.storagePriority;
        "limits.cpu" = cfg.vcpu;
        "limits.cpu.allowance" = cfg.cpuShare;
        "limits.cpu.priority" = cfg.cpuPriority;
        "limits.memory" = cfg.memorySize;
        "limits.memory.enforce" = cfg.memoryLimit;
        "limits.memory.swap" = cfg.swap;
        "limits.memory.swap.priority" = cfg.swapPriority;
        "limits.network.priority" = cfg.networkPriority;
        "raw.lxc" = concatStringsSep "\n" (mapAttrsToList formatKeyVal (
          { "lxc.apparmor.profile" = cfg.apparmorProfile; } // cfg.rawLxcConfig
        ));
      }) // cfg.extraConfig;
      devices = optionalAttrs (cfg.storagePool != null) {
        "root" = {
          type = "disk"; path = "/"; pool = cfg.storagePool;
        };
      } // optionalAttrs cfg.sharedStore {
        "store" = {
          source = "/nix/store"; path = "/nix/store";
          type = "disk"; readonly = "true";
        };
      } // listToAttrs (imap (index: network: nameValuePair "net${toString(index)}" {
        type = "nic"; nictype = "bridged"; parent = network;
      }) cfg.networks) // cfg.extraDevices;
    };
    deployment.hasFastConnection = true;
  } // baseConfiguration);
}
