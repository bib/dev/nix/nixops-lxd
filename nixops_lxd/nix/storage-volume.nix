{ config, lib, uuid, name, ... }:

with builtins;
with import ./lib.nix lib;
with lib;

{

  options = with types; {

    name = mkOption {
      default = "nixops-${uuid}-${name}";
      type = str;
      description = ''
        The name of the LXD network.
      '';
    };

    description = mkOption {
      default = "";
      type = str;
      description = ''
        The description for the LXD network.
      '';
    };

    pool = mkOption {
      default = "";
      type = resource "storageVolume";
      description = ''
        The storage pool backing this volume.
      '';
    };

    config = mkOption {
      type = attrsOf str;
      default = {};
      description = ''
        The configuration of the LXD storage volume.
      '';
    };

  };

  config = {
    _type = "storageVolume";
  };
}
