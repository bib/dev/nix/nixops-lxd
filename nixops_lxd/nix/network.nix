{ config, lib, uuid, name, ... }:

with builtins;
with import ./lib.nix lib;
with lib;

{

  options = {

    name = mkOption {
      default = "nixops-${uuid}-${name}";
      type = types.str;
      description = ''
        The name of the LXD network.
      '';
    };

    description = mkOption {
      default = "";
      type = types.str;
      description = ''
        The description for the LXD network.
      '';
    };

    config = mkOption {
      type = attrsOf str;
      default = {};
      description = ''
        The configuration of the LXD network.
      '';
    };

  };

  config = {
    _type = "network";
  };
}
