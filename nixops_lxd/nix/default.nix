{
  config_exporters = { optionalAttrs, ... }: [
    (config: { lxd = optionalAttrs (config.deployment.targetEnv == "lxd") config.deployment.lxd; })
  ];
  options = [
    ./container.nix
  ];

  resources = { evalResources, zipAttrs, resourcesByType, ... }: {
    networks = evalResources ./network.nix (zipAttrs resourcesByType.networks or []);
    storagePools = evalResources ./storage-pool.nix (zipAttrs resourcesByType.storagePools or []);
    storageVolumes = evalResources ./storage-volume.nix (zipAttrs resourcesByType.storageVolumes or []);
  };
}
