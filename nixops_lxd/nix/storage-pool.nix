{ config, lib, uuid, name, ... }:

with builtins;
with import ./lib.nix lib;
with lib;

{

  options = with types; {

    name = mkOption {
      default = "nixops-${uuid}-${name}";
      type = str;
      description = ''
        The name of the LXD storage pool.
      '';
    };

    description = mkOption {
      default = "";
      type = str;
      description = ''
        The description for the LXD storage pool.
      '';
    };

    driver = mkOption {
      type = enum ["dir" "btrfs" "lvm" "zfs" "ceph"];
      description = ''
        The driver of the LXD storage pool.
      '';
    };

    source = mkOption {
      type = str;
      description = ''
        The source of the LXD storage pool.
      '';
    };

    config = mkOption {
      type = attrsOf str;
      default = {};
      description = ''
        The configuration of the LXD storage pool.
      '';
    };

  };

  config = {
    _type = "storagePool";
  };
}
