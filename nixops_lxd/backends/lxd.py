"""NixOps backend plugin for the LXD hypervisor

This modules provides a NixOps backend that uses the LXD API to configure
containers, storage resources and network interfaces.

NOTE: This backend is a Work In Progress, many things may change.

"""
# -*- coding: utf-8 -*-
# noqa: E501
from os import environ, stat, makedirs
from os.path import exists, join, expanduser
from glob import glob
from typing import Optional
from tempfile import TemporaryDirectory
from subprocess import check_output, STDOUT
from time import sleep
from threading import Lock
from collections import defaultdict
from urllib.parse import urlsplit
from warnings import filterwarnings
import urllib3


from yaml import safe_load as yaml_load
import pylxd
from pylxd.exceptions import ClientConnectionFailed, NotFound

import nixops.known_hosts

from nixops.util import logged_exec, attr_property, create_key_pair
from nixops.backends import MachineDefinition, MachineOptions
from nixops.backends import MachineState
from nixops.resources import ResourceEval

LXD_STATUS_STOPPED = 102
LXD_STATUS_RUNNING = 103

LXD_API_WARNINGS = [
    '^Attempted to set unknown attribute "type" on instance of "Image"',
    '^Attempted to set unknown attribute "type" on instance of "Container"',
    '^Attempted to set unknown attribute "location" on instance of "Operation"',
]
for warning in LXD_API_WARNINGS:
    filterwarnings('ignore', warning)
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


USER_CERTFILE_PATH = expanduser('~/.config/lxc/client.crt')
USER_KEYFILE_PATH = expanduser('~/.config/lxc/client.key')


def format_size(size, use_kibibyte=True):
    """Format a size into IEC representation."""
    unit = 'B'
    base, suffix = [(1000., 'B'), (1024., 'iB')][use_kibibyte]
    for unit in ['B'] + [x+suffix for x in list('kMGTP')]:
        if -base < size < base:
            return "%3.1f %s" % (size, unit)
        size /= base
    return "%3.1f %s" % (size, unit)


class LxdDefinition(MachineDefinition):
    """LXD machine definition."""

    config: MachineOptions

    endpoint: Optional[str]
    ssl_cert: Optional[str]
    ssl_key: Optional[str]
    ssl_verify: Optional[bool]
    ssh_private_key: Optional[str]
    ssh_public_key: Optional[str]
    shared_store: bool
    final_config: str

    @classmethod
    def get_type(cls):
        """NixOps resource type."""
        return "lxd"

    def __init__(self, name: str, config: ResourceEval):
        super(LxdDefinition, self).__init__(name, config)
        self.name = name
        self.endpoint = config.lxd.endpoint
        self.ssl_cert = config.lxd.sslCert
        self.ssl_key = config.lxd.sslKey
        self.ssl_verify = config.lxd.sslVerify
        self.shared_store = config.lxd.sharedStore
        self.final_config = config.lxd.finalConfig


class LxdState(MachineState):  # pylint: disable=too-many-instance-attributes
    """LXD machine state."""
    private_ipv4 = attr_property('privateIpv4', None)
    private_ipv6 = attr_property('privateIpv6', None)
    endpoint = attr_property('lxd.endpoint', None)
    ssl_key = attr_property('lxd.sslKey', None)
    ssl_cert = attr_property('lxd.sslCert', None)
    ssl_verify = attr_property('lxd.sslVerify', None)
    ssh_public_key = attr_property('lxd.clientPublicKey', None)
    ssh_private_key = attr_property('lxd.clientPrivateKey', None)
    shared_store = attr_property('lxd.shared_store', None)
    base_image_hash = attr_property('lxd.imageHash', None)

    @classmethod
    def get_type(cls):
        """NixOps resource type."""
        return 'lxd'

    def __init__(self, depl, name, _id):
        super(LxdState, self).__init__(depl, name, _id)
        self.__client, self.__container = None, None
        self.__image_id = '{}-base'.format(self._vm_id())
        self.__ssh_private_key_file = None
        self.__host_ssh = nixops.ssh_util.SSH(self.logger)
        self.__host_ssh.register_host_fun(self.get_host_ssh_name)
        self.__host_ssh.register_flag_fun(self.get_host_ssh_flags)
        self.__host_ssh.register_passwd_fun(self.get_host_ssh_password)
        self.__locks = defaultdict(Lock)

    def _acquire_lock(self, key):
        self.__locks[key].acquire()

    def _release_lock(self, key):
        self.__locks[key].release()
        del self.__locks[key]

    # SSL and client parameters
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    def _make_ssl_cert(self):
        self.log_start("creating new certificate in {}...".format(self.ssl_key))
        check_output(['openssl', 'req', '-x509', '-newkey', 'rsa:2048',
                      '-keyout', self.ssl_key, '-nodes',
                      '-out', self.ssl_cert, '-subj', '/CN=nixops.local'],
                     stderr=STDOUT)
        self.log_end(" done.")

    def _export_ssl_cert(self):
        self.log_start("exporting certificate to {}...".format(self.get_host_ssh_name()))
        with open(self.ssl_cert, 'rb') as _file:
            command = ['lxc', 'config', 'trust', 'add', '/dev/stdin']
            self.__host_ssh.run_command(command, user=self.ssh_user, stdin=_file)
        self.log_end(" done.")

    def _setup_client_params(self):
        """Determine client parameters, allowing environment overrides, and generating
        the client certificates if required."""
        if 'NIXOPS_LXD_ENDPOINT' in environ:
            self.endpoint = environ['NIXOPS_LXD_ENDPOINT'] or None
        if self.endpoint is not None:
            if 'NIXOPS_LXD_SSL_KEY' in environ:
                self.ssl_key = environ['NIXOPS_LXD_SSL_KEY']
                self.ssl_cert = environ['NIXOPS_LXD_SSL_CERT']
            if 'NIXOPS_LXD_SSL_VERIFY' in environ:
                self.ssl_verify = environ['NIXOPS_LXD_SSL_VERIFY']
            if 'NIXOPS_LXD_SSL_VERIFY' in environ:
                ssl_verify = environ['NIXOPS_LXD_SSL_VERIFY'] not in ('0', '')
            if self.ssl_key is None:
                self.ssl_key = USER_KEYFILE_PATH
                self.ssl_cert = USER_CERTFILE_PATH
                if not exists(self.ssl_key):
                    self._make_ssl_cert()
                    self._export_ssl_cert()
        if self.endpoint is not None:
            if not exists(self.ssl_key):
                self.logger.error("missing client key file : {}".format(self.ssl_key))
                raise Exception("Failed to find client key file.")
            if not exists(self.ssl_cert):
                self.logger.error("missing client certificate file : {}".format(self.ssl_cert))
                raise Exception("Failed to find client certificate file.")
            if not ssl_verify:
                self.logger.warn("server certificate verification disabled")

    # Client properties
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    @property
    def _client(self):
        if self.__client is None:
            self._setup_client_params()
            try:
                if self.endpoint is None:
                    self.log_start("connecting to local daemon...")
                    self.__client = pylxd.Client()
                    self.log_end(" done.")
                else:
                    self.log_start("connecting to {}...".format(self.endpoint))
                    self.__client = pylxd.Client(
                        endpoint=self.endpoint,
                        cert=(self.ssl_cert, self.ssl_key),
                        verify=self.ssl_verify
                    )
                    self.log_end(" done.")
            except ClientConnectionFailed as err:
                self.logger.error("connection failed: {} {}".format(type(err), err))
                self.logger.error("make sure LXD is installed on the target host"
                                  " and that the HTTPS API is reachable.")
                raise Exception("Failed to connect to the hypervisor.")
            if not self.__client.trusted:
                self.logger.error("permission denied (not trusted)")
        return self.__client

    # Internal methods
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    def _evaluate(self, attr):
        path_str = 'eval-lxd-{0}-{1}'
        attr_str = 'nodes.{0}.config.deployment.lxd.{1}'
        dir_path = join(self.depl.tempdir, path_str.format(self.name, attr))
        # pylint: disable=protected-access
        args = ['nix-build'] + self.depl._eval_flags(self.depl.nix_exprs) + [
            '--arg', 'checkConfigurationOptions', 'false', '-o', dir_path,
            '-A', attr_str.format(self.name, attr)
        ]
        logged_exec(args, self.logger, capture_stdout=True).rstrip()
        paths = glob(join(dir_path, 'tarball/*'))
        assert len(paths) == 1
        return paths[0]

    def _prepare_base_image(self):
        """Ensure that the base image for this container is available."""
        self.log_start("evaluating metadata tarball...")
        metadata_path = self._evaluate('baseImageMetadata')
        self.log_end(" done.")
        self.log_start("evaluating rootfs tarball...")
        rootfs_path = self._evaluate('baseImageRootfs')
        self.log_end(" done.")
        # compute image hash to know if the image already exists in LXD's store
        image_paths = ' '.join((metadata_path, rootfs_path))
        self.log_start("computing image hash...")
        image_hash = check_output("cat {} | sha256sum".format(image_paths), shell=True)[:-4].strip().decode()
        self.log_end(" done.")
        image_size = stat(metadata_path).st_size + stat(rootfs_path).st_size
        self.log("found image : {} ({})".format(image_hash, format_size(image_size)))
        self._acquire_lock(image_hash)
        try:
            image = self._client.images.get(image_hash)  #pylint: disable=no-member
        except NotFound:
            self.log_start("importing base image...")
            with open(metadata_path, 'rb') as metadata_file:
                with open(rootfs_path, 'rb') as rootfs_file:
                    image = self._client.images.create(  #pylint: disable=no-member
                        rootfs_file, metadata=metadata_file, wait=True
                    )
            self.log_end(" done.")
            assert image.fingerprint == image_hash, "fingerprint mismatch"
        self._release_lock(image_hash)
        # add an alias to that image so that the user knows that it's managed by NixOps
        if not any(alias['name'] == self.__image_id for alias in image.aliases):
            self.log_start("adding alias...")
            image.add_alias(self.__image_id, "NixOps image alias for {}".format(self.name))
            self.log_end(" done.")
        image.save()
        return image.fingerprint

    def _cleanup_base_image(self):
        """Remove the base image for this container."""
        if self.base_image_hash is None:
            self.logger.warn("unknown base image hash, not deleting")
            return
        try:
            self.log_start("looking for image...")
            image = self._client.images.get(self.base_image_hash) #pylint: disable=no-member
            self.log_end(" found.")
        except NotFound:
            self.log_end(" not found.")
            return
        # remove our alias so that the user knows that it's no more managed by NixOps
        self._acquire_lock(self.base_image_hash)
        if any(alias['name'] == self.__image_id for alias in image.aliases):
            self.log_start("deleting alias...")
            image.delete_alias(self.__image_id)
            self.log_end(" done.")
        # delete the image only if there are no aliases remaining
        if not image.aliases:
            self.log_start("deleting image...")
            image.delete(wait=True)
            self.log_end(" done.")
        else:
            self.log_start("keeping image because it still has aliases")
        self._release_lock(self.base_image_hash)

    def _push_ssh_key(self, container):
        self.log_start("pushing SSH public key...")
        with TemporaryDirectory() as tempdir:
            # we need to build the whole hierarchy because pylxd doesn't
            # support the --parents option to files.put()
            root_dir = join(tempdir, "root")
            ssh_dir = join(root_dir, ".ssh")
            makedirs(ssh_dir)
            with open(join(ssh_dir, "authorized_keys"), "w") as file:
                file.write(self.ssh_public_key)
            container.files.recursive_put(root_dir, "/root")
        self.log_end(" done.")

    def _read_addresses(self, state):
        self.private_ipv4, self.private_ipv6 = None, None
        for iface in (state.network or {}).values():
            for address in iface['addresses']:
                if address['scope'] != "global":
                    continue
                if address["family"] == "inet" and self.private_ipv4 is None:
                    self.private_ipv4 = address["address"]
                if address["family"] == "inet6" and self.private_ipv6 is None:
                    self.private_ipv6 = address["address"]
        return self.private_ipv4 or self.private_ipv6

    def _wait_for_address(self, container):
        self.log_start("waiting for address...")
        while True:
            self._read_addresses(container.state())
            if self.private_ipv4 is not None or self.private_ipv6 is not None:
                break
            sleep(1)
            self.log_continue(".")
        self.log_end(" done.")
        if self.private_ipv4 is not None:
            self.log("IPv4 address: {}".format(self.private_ipv4))
        if self.private_ipv6 is not None:
            self.log("IPv6 address: {}".format(self.private_ipv6))

    # Plugin implementation
    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    # Resource, physical spec
    # -------------------------------------------------------------------------

    def _vm_id(self):
        return "nixops-{0}-{1}".format(self.depl.uuid, self.name)

    def get_physical_spec(self):
        """NixOps physical specification."""
        return {
            ('users', 'extraUsers', 'root', 'openssh', 'authorizedKeys', 'keys'):
            [self.ssh_public_key]
        }

    # SSH callbacks
    # -------------------------------------------------------------------------

    def get_ssh_private_key_file(self):
        """Get the path to the SSH private key, writing it if necessary."""
        if self.__ssh_private_key_file is None:
            self.__ssh_private_key_file = self.write_ssh_private_key(self.ssh_private_key)
        return self.__ssh_private_key_file

    def get_ssh_name(self):
        """Get the container's SSH connection hostname."""
        assert self.private_ipv4 or self.private_ipv6
        return self.private_ipv4 or self.private_ipv6

    def get_ssh_flags(self, scp=False):
        """Get the container's SSH connection flags."""
        super_flags = super(LxdState, self).get_ssh_flags(scp=scp)
        return super_flags + ["-o", "StrictHostKeyChecking=accept-new",
                              "-i", self.get_ssh_private_key_file()]

    def get_host_ssh_name(self):
        """Get the hypervisor's SSH hostname."""
        if self.endpoint:
            default_host = urlsplit(self.endpoint)[1].split(':')[0]
        else:
            default_host = 'localhost'
        return environ.get('NIXOPS_LXD_TARGET_NAME') or default_host

    def get_host_ssh_flags(self, scp=False):
        """Get the hypervisor's SSH connection flags."""
        port = environ.get('NIXOPS_LXD_TARGET_PORT') or self.ssh_port
        return [
            "-P" if scp else "-p", str(port),
            "-o", "StrictHostKeyChecking=accept-new",
            "-i", self.get_ssh_private_key_file()
        ]

    @staticmethod
    def get_host_ssh_password():
        """Get the hypervisor's SSH connection password."""
        # TODO: use getpass
        return None

    def get_ssh_for_copy_closure(self):
        """Get the SSH connection used to copy the system closure."""
        # XXX: when https://github.com/NixOS/nixops/pull/1353 is merged, we'll
        #      have to define our own Transport with copy_closure() depending
        #      on shared_store
        if bool(int(self.shared_store)):
            return self.__host_ssh
        return self.ssh

    # Container lifetime
    # -------------------------------------------------------------------------

    def start(self):
        """Start a container, if possible."""
        container = self._client.containers.get(self.vm_id)  # pylint: disable=no-member
        self.log_start("starting...")
        state = container.state()
        if state.status == 'Running':
            self.log_end(" already running.")
            self._read_addresses(state)
            return
        container.start(wait=True)
        while True:
            state = container.state()
            if state.status_code == LXD_STATUS_STOPPED:
                raise Exception("Container stopped unexpectedly.")
            if state.status_code == LXD_STATUS_RUNNING:
                break
            sleep(1)
            self.log_continue(".")
        self.log_end(" done.")
        self._wait_for_address(container)

    def stop(self):
        """Stop a container, if possible."""
        self.log_start("shutting down...")
        try:
            container = self._client.containers.get(self.vm_id)  # pylint: disable=no-member
        except NotFound:
            self.log_end(" not found.")
            self.logger.error("cannot stop : container already removed")
            return
        if container.state().status == 'Stopped':
            self.log_end(" not running.")
            return
        container.stop(wait=True)
        self.log_end(" done.")

    def create(self, definition, check, allow_reboot, allow_recreate):
        """Create a new container, initializing its state from the definition."""
        # TODO: implement check argument
        # TODO: implement allow_reboot argument
        # TODO: implement allow_recreate argument
        assert isinstance(definition, LxdDefinition)
        if self.ssh_private_key is None:
            self.ssh_private_key, self.ssh_public_key = create_key_pair()
        # initialize container state
        self.set_common_state(definition)
        self.endpoint = definition.endpoint
        self.ssl_key = definition.ssl_key
        self.ssl_cert = definition.ssl_cert
        self.ssl_verify = definition.ssl_verify
        self.shared_store = definition.shared_store
        # prepare the base image for this container
        if self.vm_id is None:
            vm_id = self._vm_id()
            self.base_image_hash = self._prepare_base_image()
            self.log_start("loading container config...")
            config = yaml_load(definition.final_config)
            self.log_end(" done.")
            self.log_start("creating new container...")
            container = self._client.containers.create(dict(  # pylint: disable=no-member
                name=vm_id,
                source=dict(type='image', alias=self.__image_id),
                profiles=config['profiles'],
                config=config['config'],
                devices=config['devices'],
            ), wait=True)
            self.log_end(" done.")
            try:
                self._push_ssh_key(container)
            except Exception:
                container.delete(wait=True)
                raise
            self.vm_id = vm_id
        self.start()
        return True

    def switch_to_configuration(self, method, sync, command=None) -> int:
        """
        Override MachineState.switch_to_configuration() to run reload D-Bus
        before switching to the new configuration.
        """
        self.run_command("/run/current-system/sw/bin/systemctl reload dbus")
        return super().switch_to_configuration(method, sync, command=command)

    def destroy(self, wipe=False):
        # TODO: implement wipe argument
        """Destroy a container and cleanup its base image."""
        self.stop()
        container = self._client.containers.get(self.vm_id)  # pylint: disable=no-member
        if container is not None:
            self.log_start("deleting...")
            container.delete()
            self.log_end(" done.")
        self._cleanup_base_image()
        return True
